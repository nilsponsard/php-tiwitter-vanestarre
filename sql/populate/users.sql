DELETE FROM Users WHERE true;

INSERT INTO Users (user_id, email, passwd, type) value ('admin', 'admin@exemple.com', SHA1('admin'), 'admin');
INSERT INTO Users (user_id, email, passwd, type) value ('test1', 'test1@exemple.com', SHA1('test'), 'member');
INSERT INTO Users (user_id, email, passwd, type) value ('test2', 'test2@exemple.com', SHA1('test'), 'member');
INSERT INTO Users (user_id, email, passwd, type) value ('test3', 'test3@exemple.com', SHA1('test'), 'member');
INSERT INTO Users (user_id, email, passwd, type) value ('test4', 'test4@exemple.com', SHA1('test'), 'member');
INSERT INTO Users (user_id, email, passwd, type) value ('hugo', 'hugo.tritz@etu.univ-amu.fr', SHA1('hhh'), 'member');

SELECT * FROM Users;