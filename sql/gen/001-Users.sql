create or replace table Users
(
	user_id varchar(255) not null
		primary key,
	email varchar(255) not null,
	passwd varchar(255) not null,
    type varchar(255) not null,
    pass_reset_code varchar(255) null,
	constraint mail
		unique (email)
);

