create or replace table Messages
(
	message_id int auto_increment
		primary key,
	texte varchar(50) not null,
	image_present tinyint(1) not null,
	nb_love_max int not null,
	nb_love int not null,
	nb_cute int not null,
	nb_style int not null,
	nb_swag int not null
);

