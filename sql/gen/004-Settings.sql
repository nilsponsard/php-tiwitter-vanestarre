create or replace table Settings
(
    setting_id    varchar(40)
        primary key,
    setting_value varchar(40) not null
);
