# projet php vanestarre
[http://mickael-martin-nevot.com/univ-amu/iut/dut-informatique/programmation-web-cote-serveur/?s24-projet.pdf](http://mickael-martin-nevot.com/univ-amu/iut/dut-informatique/programmation-web-cote-serveur/?s24-projet.pdf)

projet S4 de php avec : 
- Baptiste DST
- Hugo T.
- Nils P.

# standards à utiliser : 
- style de nommage : snake_case
- variables commencent par une minuscule 
- Classes commencent par une majuscule
- CONSTANTES ont toutes leurs lettres en majuscule
- le nom des variables doit être en anglais



# fonctionnalités demandées 
## Poster des messages depuis le compte admin
- 50 caractères 
- pouvant contenir plusieurs images (ou aucune)
- catégories de messages avec le β (possible après l’envoi du message)
- détermination d’un nombre de reaction *love* à obtenir
    - quand ce nombre est atteint, le dernier à avoir mis cette réaction devra donner de l’argent à vanestarre
### réaction sur un message 
- réservé aux inscrits
- 4 émojis : *love*,*cute*,trop stylé,*swag*
- un seul emoji par utilisateur pour un message

## recherche par tag
## création de comptes invités
- donne accès aux réactions
- informations : 
    - pseudo
    - mot de passe (à sécuriser)
    - email
- page de création de compte

## niveaux d’authorisation
du moins privilégié au plus privilégié (garde les privilèges d’au dessus)
- visiteur (pas de compte)
    + lecture des messages
- membre (compte)
    + ajout de réaction
- administrateur 
    + suppression de messages
    + ajout de messages
    + modification des messages


# organisation de la base de donnée 
Users (**user_id**, mail, passwd, type)  
Messages (**id**, texte, image_present, *id_list_tag* ,*id_list_image*, nb_love_max, nb_love,nb_cute,nb_style,nb_swag)  
List_image (**id_list_image**, image_path )  
List_tag (**id_list_tag**, tag_name)  
Settings (**setting_id**,setting_value)

# design de l’interface
[https://app.diagrams.net/#Anilsponsard%2Fphp-tiwitter-vanestarre%2Fmaster%2Finterface.drawio](https://app.diagrams.net/#Anilsponsard%2Fphp-tiwitter-vanestarre%2Fmaster%2Finterface.drawio)

