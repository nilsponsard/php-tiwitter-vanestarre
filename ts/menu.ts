let menu = document.getElementById("menu") as HTMLDivElement
let button_menu = document.getElementById("button_menu") as HTMLDivElement
let img_menu = document.getElementById("img_menu") as HTMLImageElement
button_menu.addEventListener("click", () => {
    if (menu.classList.contains("menu_hide")) { // if the menu is closed
        img_menu.src = "assets/cross.svg"
        menu.classList.remove("menu_hide")
    } else {

        menu.classList.add("menu_hide")
        img_menu.src = "assets/menu.svg"
    }
})