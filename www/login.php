<?php
include 'lib/db.inc.php';
include 'lib/status.inc.php';

$status = new Status();


session_start();
if (isset($_SESSION['Connection'])) {
    header('location:index.php');
    die();
}


$pdo = getPdo();

if (isset($_POST['username']) && isset($_POST['password'])) {

    $sql = 'SELECT user_id, email, type FROM Users WHERE user_id = :username AND passwd = SHA1(:password)';
    $stmt = $pdo->prepare($sql);
    $stmt->bindValue('username', $_POST['username'], PDO::PARAM_STR);
    $stmt->bindValue('password', $_POST['password'], PDO::PARAM_STR);

    try {
        $stmt->execute();
        if ($stmt->rowCount() == 1) {
            $stmt->setFetchMode(PDO::FETCH_OBJ);
            $result = $stmt->fetch();
            $_SESSION['Connection'] = $result;
            header('location:index.php');
            die();
        } else {
            $status->add_event("la connexion a échoué", true);
        }
    } catch (PDOException $e) {
        //echo 'Erreur : ', $e->getMessage(), PHP_EOL;
        //echo 'Requête : ', $sql, PHP_EOL;
        //exit();
        $status->add_event("la connexion a échoué", true);
    }
}
include 'partial/head.part.php';

?>
    <div class="content">

        <?php $status->show(); ?>
        <form method="post" action="#">
            <label for="username">Identifiant:</br></label><input type="text" name="username" id="username"
                                                                  value="<?php if (isset($_POST['username'])) {
                                                                      echo $_POST['username'];
                                                                  } ?>" required/><br/>
            <label for="password">Password:</br></label><input type="password" name="password" id="password"
                                                               required/><br/> <br/>
            <input type="submit" name="Login" id="submit" value="Log In"/>
        </form>

        <a href="forgottenPassword.php">forgotten password</a>
    </div>

<?php
include 'partial/foot.part.php';
