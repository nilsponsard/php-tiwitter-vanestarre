<?php
include 'lib/db.inc.php';

session_start();
if(isset($_SESSION['Connection']) || !isset($_GET['code'])){
    header('location:index.php');
    die();
}

$password_invalid = false;
$pdo = getPdo();

if(isset($_POST['password'], $_POST['password2'], $_POST['code'])) {
    if($_POST['password'] == $_POST['password2']) {

        $sql = 'UPDATE Users SET passwd = sha1(:passwd), pass_reset_code = null WHERE pass_reset_code = :code';
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue('passwd', $_POST['password'], PDO::PARAM_STR);
        $stmt->bindValue('code', $_POST['code'], PDO::PARAM_STR);

        try {
            $stmt->execute();
            if ($stmt->rowCount() == 1) {
                header('location:login.php');
                die();
            }
        } catch (PDOException $e) {
            //echo 'Erreur : ', $e->getMessage(), PHP_EOL;
            //echo 'Requête : ', $sql, PHP_EOL;
            //exit();
        }
    } else
        $password_invalid = true;
}

?>


<?php if($password_invalid) echo '<h1>Password not equals !!!!</h1>'?>

<form method="post" action="#">
    <label for="password" >Password:</br></label><input type="password" name="password" id="password" required /></br ></br >
    <label for="password2" >Confirm password:</br></label><input type="password" name="password2" id="password2" required /></br ></br >
    <input type="hidden" name="code" value="<?= $_GET['code'] ?>">
    <input type="submit" name="reset" id="submit" value="Reset"/>
</form>