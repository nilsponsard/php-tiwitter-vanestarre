<?php
session_start();
include 'lib/status.inc.php';
$user_id = null;
$status = new Status();
if (isset($_POST["old_user_id"])) {
    $user_id = $_POST["old_user_id"];
} else if (isset($_GET["user_id"])) {
    $user_id = $_GET["user_id"];
    if (isset($_GET["error"])) {
        $status->add_event($_GET["error"], true);
    }
}


if (!isset($user_id)) {
    if (isset($_SESSION["Connection"])) {
        header('location:profile.php?user_id=' . $_SESSION["Connection"]->user_id);
        die();
    } else {// kick to index.php if not logged in and don’t try to see a profile
        header('location:login.php');
        die();
    }
}
include 'lib/db.inc.php';
$pdo = getPdo();


// if updating a profile
if (isset($_POST["update"]) && $_POST["update"] == "update") {

    if ($_SESSION["Connection"]->user_id != $_POST["old_user_id"]) {
        $status->add_event("Unknown error", true); // can’t modify another user
    } else {
        $new_user_id = $_POST["user_id"];
        $new_email = $_POST["email"];
        $old_password = $_POST["password"];
        $new_password = $_POST["new_password"];
        $confirm_new_password = $_POST["confirm_new_password"];
        if ($new_password != $confirm_new_password) {
            $status->add_event("les nouveaux mots de passe doivent être identiques", true);
        } else {
            if ($new_password == "") {
                $new_password = $old_password;
            }
            $sql_modify = 'UPDATE Users
                                    SET user_id = :new_user_id, email = :email , passwd= SHA1(:new_password)
                                    WHERE user_id = :old_user_id and passwd = SHA1(:old_password)';
            $stmt_modify = $pdo->prepare($sql_modify);
            $stmt_modify->bindValue('new_user_id', $new_user_id, PDO::PARAM_STR);
            $stmt_modify->bindValue('email', $new_email, PDO::PARAM_STR);
            $stmt_modify->bindValue('new_password', $new_password, PDO::PARAM_STR);
            $stmt_modify->bindValue('old_password', $old_password, PDO::PARAM_STR);
            $stmt_modify->bindValue('old_user_id', $user_id, PDO::PARAM_STR);

            try {
                $stmt_modify->execute();


                if ($stmt_modify->rowCount() == 1) {
                    header('location:logout.php');
                    die();
                } else {
                    header('location:profile.php?user_id=' . $user_id . '&error=mot de passe incorrect');
                    die();
                }
            } catch (PDOException $e) {

                $status->add_event("error lors de la modification", true);
            }
        }
    }

}


include 'partial/head.part.php';
?>

<?php


$sql = 'SELECT user_id, email, type FROM Users WHERE user_id = :username';
$stmt = $pdo->prepare($sql);
$stmt->bindValue('username', $user_id, PDO::PARAM_STR);
$connectionFailed = false;
$result = null;
try {
    $stmt->execute();
    if ($stmt->rowCount() == 1) {
        $stmt->setFetchMode(PDO::FETCH_OBJ);
        $result = $stmt->fetch();
    } else {
        $connectionFailed = true;
    }
} catch (PDOException $e) {
    $connectionFailed = true;
}
?>
    <div class="content">
        <div class="status">
            <?php $status->show(); ?>
        </div>
        <div class="infos">

            <?php if ($connectionFailed) : ?>
                <p>
                    Compte introuvable.
                    <a href='index.php'>retour à l’accueil</a>

                </p>

            <?php else : ?>

                <?php if ($result->user_id == $_SESSION["Connection"]->user_id): ?>

                    <form action='profile.php' method='POST'>
                        <label>
                            <input type='text' class='hidden' name='old_user_id' value='<?= $user_id ?>'>
                        </label>
                        <ul>
                            <li><label> identifiant : <input name='user_id' type='text' value='<?= $result->user_id ?>'>
                                </label></li>


                            <li><label> email : <input name='email' type='text' value='<?= $result->email ?>'> </label>
                            </li>


                            <li>type : <?= $result->type ?></li>

                            <li><label> old password: <input type='password' name='password'> </label></li>

                            <li><label> new password: <input type='password' name='new_password'> </label></li>

                            <li><label> confirm new password: <input type='password' name='confirm_new_password'>
                                </label></li>

                        </ul>

                        <input type='submit' name='update' value='update'>

                    </form>

                <?php else: ?>
                    <ul>

                        <li> identifiant : <?= $result->user_id ?></li>

                        <li> email : <?= $result->email ?></li>

                        <li> type : <?= $result->type ?></li>
                    </ul>

                <?php endif; ?>
            <?php endif; ?>
        </div>

    </div>

<?php
include 'partial/foot.part.php'
?>