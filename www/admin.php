<?php
session_start();
include 'lib/status.inc.php';
$status = new Status();
if (!isset($_SESSION["Connection"])) {
    header('location:login.php');
    die();
}

// now we assume that we got a session

if ($_SESSION["Connection"]->type != "admin") {
    header('location:index.php');
    die();
}

// now we assume that the user is an admin

include 'lib/db.inc.php';
$pdo = getPdo();
$min_love = 0;
$max_love = 0;
if (isset($_POST["update_settings"])) {
    $min_love = intval($_POST["min_love"]);
    $max_love = intval($_POST["max_love"]);
    if ($min_love > 0 and $max_love > 0) {
        if ($min_love <= $max_love) { // <= so we can force the randomness
            $sql_change_love = 'UPDATE Settings
                                    SET setting_value = :setting_value
                                    WHERE setting_id = :setting_id ';
            $stmt_max = $pdo->prepare($sql_change_love);
            $stmt_max->bindValue('setting_value', $max_love, PDO::PARAM_STR);
            $stmt_max->bindValue('setting_id', "max_love", PDO::PARAM_STR);

            $stmt_min = $pdo->prepare($sql_change_love);
            $stmt_min->bindValue('setting_value', $min_love, PDO::PARAM_STR);
            $stmt_min->bindValue('setting_id', "min_love", PDO::PARAM_STR);

            $pdo->beginTransaction();
            try {
                $stmt_max->execute();
                $stmt_min->execute();
                $pdo->commit();
                $status->add_event("modifications effectuées avec succès", false);
            } catch (Exception $e) {
                $pdo->rollBack();
                $status->add_event("erreur lors de la modification", true);
            }

        } else {
            $status->add_event("min doit être inférieur à max", true);
        }
    } else {
        $status->add_event("valeurs incorrectes", true);
    }

}
$sql = 'SELECT setting_id,setting_value FROM Settings';
$stmt = $pdo->prepare($sql);
try {
    $stmt->execute();
    if ($stmt->rowCount() == 2) {
        $stmt->setFetchMode(PDO::FETCH_OBJ);
        $result = $stmt->fetchAll();
        foreach ($result as $value) {
            if ($value->setting_id == "max_love") {
                $max_love = $value->setting_value;
            }
            if ($value->setting_id == "min_love") {
                $min_love = $value->setting_value;
            }
        }
    } else {
        $status->add_event("database error", true);
    }
} catch (PDOException $e) {
    //echo 'Erreur : ', $e->getMessage(), PHP_EOL;
    //echo 'Requête : ', $sql, PHP_EOL;
    //exit();
    $status->add_event("database error ", true);

}
include 'partial/head.part.php';

?>


<div class="content">
    <?php
    $status->show();
    ?>
    <h1>outils :</h1>
    <ul>
        <li><a href="#send_message">envoyer un message</a></li>
        <li><a href="#website_settings">options du site</a></li>
    </ul>

    <h2 id="send_message">
        envoyer un message</h2>
    <form action="admin.php" method="post">
        <label>
            contenu
            <br/>
            <textarea name="text" placeholder="50 caractères max"></textarea>
        </label>
        <br/>
        <label>
            tags:
            <br/>
            <textarea name="tags" placeholder="séparer les tags par des virgules"></textarea>
        </label>
        <br/>
        <label>
            insérer des images : sélection multiple possible <br/>
            <input type="file" multiple accept="image/*,.jpg,.jpeg,.gif,.png,.bmp">
        </label>
        <br/>
        <input type="submit" name="send_message" value="envoyer">
    </form>

    <h2 id="website_settings">options du site</h2>
    <form action="admin.php" method="post">

        <label>
            min love : <input type="number" name="min_love" value="<?= $min_love ?>">
        </label>
        <br/>
        <label>
            max love : <input type="number" name="max_love" value="<?= $max_love ?>">
        </label> <br/s>

        <input type="submit" name="update_settings" value="mettre à jour">
    </form>

</div>
