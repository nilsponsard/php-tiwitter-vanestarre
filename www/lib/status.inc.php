<?php

class Status_event
{
    private string $message;
    private bool $error; // "info" or "error"

    public function __construct(string $message, bool $is_error)
    {
        $this->message = $message;
        $this->error = $is_error;
    }

    public function show()
    {
        $classes = "event";
        if ($this->error) {
            $classes .= " error";
        }
        echo "<div class='$classes'>
               " . $this->message . "
                </div>";
    }

}

class Status
{
    private array $messages;

    public function __construct()
    {
        $this->messages = array();
    }

    public function add_event(string $message, bool $is_error = false)
    {

        $this->messages[] = new Status_event($message, $is_error);
    }

    public function show()
    {
        foreach ($this->messages as $message) {
            $message->show();
        }
    }

}