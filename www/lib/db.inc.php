<?php

function getPdo()
{
    $dbUsername = getenv('DATABASE_USERNAME');
    $dbPassword = getenv('DATABASE_PASSWORD');
    $dbName = getenv('DATABASE_NAME');
    $dbHost = getenv('DATABASE_HOST');

    if (!$dbUsername || !$dbPassword || !$dbName || !$dbHost)
        include 'dbCred.inc.php';

    try {
        $dsn = 'mysql:host=' . $dbHost . ';dbname=' . $dbName;
        $pdo = new PDO($dsn, $dbUsername, $dbPassword);
    } catch (PDOException $e) {
        die('Erreur : ' . $e->getMessage());
    }
    return $pdo;
}