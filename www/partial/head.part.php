<?php
if (!isset($_SESSION)) {
    session_start();
}
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>tiwitter</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<!--pour remonter tout en haut-->
<div id="top"></div>


<header class="header">
    <div class="nav_bar">
        <a href="#top" id="button_top"><img src="assets/v_for_vanestarre.svg" alt="vers le haut"></a>
        <div><a href="index.php#top"> VANESTARRE </a></div>
        <div id="button_menu"><img id="img_menu" src="assets/menu.svg" alt="menu"></div>
        <div id="menu" class="menu_hide">

            <?php
            if (isset($_SESSION['Connection'])) : ?>
                <?php if ($_SESSION["Connection"]->type == "admin") : ?>
                    <a href="admin.php" class="h_link1">admin</a>
                <?php endif; ?>
                <a href="logout.php" class="h_link2">déconnexion</a>
                <a href="profile.php" class="h_link3">profil</a>

            <?php else : ?>

                <a href="login.php" class="h_link2">connexion</a><br>
                <a href="signup.php" class="h_link3">inscription</a>

            <?php endif; ?>

        </div>
    </div>

    <!--menu needs to be loaded before this script-->
    <script src="js/menu.js"></script>

</header>