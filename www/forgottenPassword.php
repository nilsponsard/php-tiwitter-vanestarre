<?php
include 'lib/db.inc.php';

session_start();
if(isset($_SESSION['Connection'])){
    header('location:index.php');
    die();
}

$mail_sent = false;

if(isset($_POST['email'])) {
    $mail_sent = true;

    //$sql = 'SELECT user_id, email FROM Users WHERE email = :email';
    $sql = 'UPDATE Users SET pass_reset_code = :reset_code WHERE email = :email';
    $stmt = $pdo->prepare($sql);
    $stmt->bindValue('email', $_POST['email'], PDO::PARAM_STR);
    $reset_code = uniqid();
    $stmt->bindValue('reset_code', $reset_code, PDO::PARAM_STR);

    try {
        $stmt->execute();
        if($stmt->rowCount() == 1) {
            $msg = 'Follow the link to reset your password: <br><a href="https://' . $_SERVER['HTTP_HOST'] . '/resetPassword.php?code=' . $reset_code . '">reset password</a>';
            mail($_POST['email'], 'Password reset', $msg);
            echo '<p> DEBUG', $msg,'</p>';
        }
    } catch (PDOException $e) {
        //echo 'Erreur : ', $e->getMessage(), PHP_EOL;
        //echo 'Requête : ', $sql, PHP_EOL;
        //exit();
    }
}


?>

<?php if($mail_sent): ?>
    <h2>si un compte correspond a l'addresse email, un email avec le lien de réinitialisation vous sera envoyer.</h2>
<?php endif; ?>

<form method="post" action="#">
    <label for="email" >E-mail:</br></label><input type="email" name="email" id="email" required /></br >
    <input type="submit" name="submit" id="submit" value="Send new password"/>
</form>
