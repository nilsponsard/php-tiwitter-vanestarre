<?php
include 'lib/db.inc.php';
include 'lib/status.inc.php';
session_start();
if (isset($_SESSION['Connection'])) {
    header('location:index.php');
    die();
}

$status = new Status();
$pdo = getPdo();

if (isset($_POST['username'], $_POST['email'], $_POST['password'], $_POST['password2'])) {
    if ($_POST['password'] == $_POST['password2']) {

        $sql = 'INSERT INTO Users (user_id, email, passwd, type) value ( :user_id, :email, SHA1(:passwd), \'member\')';
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue('user_id', $_POST['username'], PDO::PARAM_STR);
        $stmt->bindValue('email', $_POST['email'], PDO::PARAM_STR);
        $stmt->bindValue('passwd', $_POST['password'], PDO::PARAM_STR);

        try {
            $stmt->execute();
            if ($stmt->rowCount() == 1) {
                header('location:login.php');
                die();
            }
        } catch (PDOException $e) {
            //echo 'Erreur : ', $e->getMessage(), PHP_EOL;
            //echo 'Requête : ', $sql, PHP_EOL;
            //exit();
        }
    } else
        $status->add_event("les mots de passe ne sont pas identiques", true);
}

include 'partial/head.part.php'

?>


    <div class="content">
        <?php $status->show(); ?>


        <form method="post" action="#">
            <label for="username">Identifiant:</br></label><input type="text" name="username" id="username"
                                                                  value="<?php if (isset($_POST['username'])) {
                                                                      echo $_POST['username'];
                                                                  } ?>" required/><br/>
            <label for="email">E-mail:</br></label><input type="email" name="email" id="email"
                                                          value="<?php if (isset($_POST['email'])) {
                                                              echo $_POST['email'];
                                                          } ?>" required/><br/>
            <label for="password">Password:</br></label><input type="password" name="password" id="password"
                                                               required/><br/>
            <label for="password2">Confirm password:</br></label><input type="password" name="password2" id="password2"
                                                                        required/><br/>
            <input type="submit" name="signup" id="submit" value="Sign Up"/>
        </form>
    </div>

<?php
include 'partial/foot.part.php';
