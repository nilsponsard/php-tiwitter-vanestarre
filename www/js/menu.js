"use strict";
var menu = document.getElementById("menu");
var button_menu = document.getElementById("button_menu");
var img_menu = document.getElementById("img_menu");
button_menu.addEventListener("click", function () {
    if (menu.classList.contains("menu_hide")) { // if the menu is closed
        img_menu.src = "assets/cross.svg";
        menu.classList.remove("menu_hide");
    }
    else {
        menu.classList.add("menu_hide");
        img_menu.src = "assets/menu.svg";
    }
});
